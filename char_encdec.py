import time
import sys
import numpy as np
from theano import tensor as T
from collections import Counter
import cPickle as pickle
from utils import save_model_params, load_model_params
from opt import optimizers
from theano.printing import pydotprint
from cfg import CHAR_MAX_SEQ_LEN, CHAR_EOS_TOK, CHAR_SOS_TOK, CHAR_UNK_TOK, CHAR_SORT_K_BATCHES, CLIP_RATIO
from os.path import join as pjoin
from encdec_shared import EncoderDecoder, reverse_sent, pyrpad, clip_lengths
from run_utils import setup_exp
from char_fuel import load_parallel_data
from cfg import TRAIN_FILES, VALID_FILES

def seq_to_str(seq, bdict):
    return ''.join([bdict[k] for k in seq])

# architectural changes
# add bi-directionality again
def construct_dicts(text_file):
    with open(text_file, 'r') as fin:
        text = fin.read()
    counts = Counter(text)
    fdict = dict()
    for idx, val in enumerate(counts):
        fdict[val] = idx
    #fdict['"'] = len(fdict)   # FIXME since " replaced w/ `` in lang8
    fdict[CHAR_EOS_TOK] = len(fdict)
    fdict[CHAR_SOS_TOK] = len(fdict)
    fdict[CHAR_UNK_TOK] = len(fdict)
    bdict = {v: k for k, v in fdict.iteritems()}
    return fdict, bdict

def sample_output(encdec, bdict, fdict, hs=None):
    CHAR_SOS_IND = fdict[CHAR_SOS_TOK]
    CHAR_EOS_IND = fdict[CHAR_EOS_TOK]
    sample = ''
    s_t = np.array([CHAR_SOS_IND,], dtype=np.int32)
    j = 0
    aligns = list()
    h_ps = [np.zeros(hs[0].shape, dtype=np.float32) for k in xrange(encdec.rlayers)]
    while (j == 0 or s_t != CHAR_EOS_IND) and j < CHAR_MAX_SEQ_LEN:
        if encdec.attention:
            ret = encdec.decode_step(*([s_t] + h_ps + [hs]))
        else:
            ret = encdec.decode_step(*([s_t] + h_ps))
        p_t = ret[0]
        if encdec.attention:
            h_ps = ret[1:-1]
            align = ret[-1]
            aligns.append(align.T)
        else:
            h_ps = ret[1:]
        h_ps = [h_p for h_p in h_ps]
        #s_t = np.argmax(np.random.multinomial(1, p_t.flatten()))
        # NOTE taking argmax instead of sampling
        s_t = np.array([np.argmax(p_t.flatten()),], dtype=np.int32)
        sample = sample + bdict[s_t[0]]
        j = j + 1
    if encdec.attention:
        aligns = np.concatenate(aligns, axis=0)
        return sample, aligns
    else:
        return sample

if __name__ == '__main__':
    import json
    import argparse
    import logging
    parser = argparse.ArgumentParser()
    parser.add_argument('--bidir', action='store_true', help='use bidirectional encoder')
    parser.add_argument('--pyramid', action='store_true', help='use pyramid layers')
    parser.add_argument('--rnn_dim', type=int, default=400, help='dimension of recurrent states')
    parser.add_argument('--rlayers', type=int, default=2, help='number of hidden layers for RNNs')
    parser.add_argument('--batch_size', type=int, default=128, help='size of batches')
    parser.add_argument('--lr', type=float, default=0.001, help='learning rate')
    parser.add_argument('--lr_decay', type=float, default=0.9, help='learning rate decay')
    parser.add_argument('--lr_decay_after', type=int, default=1, help='epoch after which to decay')
    parser.add_argument('--max_norm', type=float, default=1.0, help='norm at which to rescale gradients')
    parser.add_argument('--dropout', type=float, default=0.0, help='dropout (fraction of units randomly dropped on non-recurrent connections)')
    parser.add_argument('--epochs', type=int, default=20, help='number of epochs to train')
    parser.add_argument('--print_every', type=int, default=1, help='how often to print cost')
    parser.add_argument('--optimizer', type=str, default='adam', choices=optimizers)
    parser.add_argument('--reverse', action='store_true', help='reverse source input sentence')
    parser.add_argument('--attention', action='store_true', help='use attention over encoder hidden states')
    parser.add_argument('--expdir', type=str, default='sandbox', help='experiment directory to save files to')
    parser.add_argument('--resume_epoch', type=int, default=None, help='resume starting at this epoch (expdir must have saved models, and this will also load previous opts.json for configuration)')
    args = parser.parse_args()

    fdict, bdict = construct_dicts(TRAIN_FILES[0])

    args.src_vocab_size = args.tgt_vocab_size = len(fdict)
    args.max_seq_len = CHAR_MAX_SEQ_LEN; args.sort_k_batches = CHAR_SORT_K_BATCHES
    args.src_train_file, args.tgt_train_file = TRAIN_FILES
    args.src_valid_file, args.tgt_valid_file = VALID_FILES

    logger, opts = setup_exp(args)
    # save dictionary
    with open(pjoin(args.expdir, 'dicts.pk'), 'wb') as fout:
        pickle.dump({'fdict': fdict, 'bdict': bdict}, fout)

    logger.info('setting up data...')
    train_stream = load_parallel_data(TRAIN_FILES[0], TRAIN_FILES[1], args.batch_size, CHAR_SORT_K_BATCHES, fdict, training=True)
    bsize = args.batch_size if not args.attention else args.batch_size / CLIP_RATIO
    valid_stream = load_parallel_data(VALID_FILES[0], VALID_FILES[1], bsize, CHAR_SORT_K_BATCHES, fdict, training=False)
    logger.info('done setting up data')

    if args.resume_epoch:
        assert (args.resume_epoch > 0)
        start_epoch = args.resume_epoch
        logger.info('loading model...')
        encdec = EncoderDecoder(args, attention=args.attention, bidir=args.bidir, subset_grad=False, pyramid=args.pyramid)
        load_model_params(encdec, pjoin(args.expdir, 'model_epoch%d.h5' % (args.resume_epoch - 1)))
        logger.info('done loading model')
    else:
        start_epoch = 0
        encdec = EncoderDecoder(args, attention=args.attention, bidir=args.bidir, subset_grad=False, pyramid=args.pyramid)
        logger.info('# params: %d' % encdec.nparams)

    # print graph for debugging (encdec.train should be optimized,
    # print encdec.cost for unoptimized)
    #pydotprint(encdec.train, outfile=pjoin(args.expdir, 'model.png'), var_with_name_simple=True)

    lr = args.lr
    print(fdict)

    # TODO integrate pretty printing like https://gist.github.com/hojonathanho/006230ccde2adbe1cf5c

    for epoch in xrange(start_epoch, args.epochs):
        if epoch >= args.lr_decay_after and args.optimizer not in ['adam']:
            lr = lr * args.lr_decay
            logger.info('decaying learning rate to: %f' % lr)
        exp_length = None
        exp_cost = None
        exp_norm = None

        it = 0
        for ss, sm, ts, tm in train_stream.get_epoch_iterator():
            try:
                tic = time.time()
                it = it + 1
                ss, sm = pyrpad(ss, sm, args.rlayers, args.pyramid)
                ss, ts = ss.astype(np.int32), ts.astype(np.int32)
                sm, tm = sm.astype(np.bool), tm.astype(np.bool)
                rss = reverse_sent(ss, sm)
                if it == 1:
                    # FIXME assuming this will be upper limit
                    exp_norm = float(1e6)
                cost, grad_norm, param_norm = encdec.train(ss, sm, rss, ts, tm, None, args.dropout, lr, args.max_norm)
                # normalize by average length when printing
                # note exclude the first token for computing cost
                lengths = np.sum(tm[:, 1:], axis=1)
                mean_length = np.mean(lengths)
                std_length = np.std(lengths)
                if not exp_cost:
                    exp_cost = cost
                    exp_length = mean_length
                    exp_norm = grad_norm
                else:
                    exp_cost = 0.99*exp_cost + 0.01*cost
                    exp_length = 0.99*exp_length + 0.01*mean_length
                    exp_norm = 0.99*exp_norm + 0.01*grad_norm
                cost = cost / mean_length
                toc = time.time()
                if it % args.print_every == 0:
                    logger.info('epoch %d, iter %d, cost %f, expcost %f, grad/param norm %f, batch time %f, length mean/stdev %f/%f' %\
                            (epoch + 1, it, cost, exp_cost/exp_length, grad_norm/param_norm, toc - tic, mean_length, std_length))
            except KeyboardInterrupt:
                confirm = raw_input('Are you sure you want to quit in middle of training? (Enter "yes" to quit)')
                if confirm == 'yes':
                    sys.exit()
                else:
                    continue

        logger.info('saving model')
        save_model_params(encdec, pjoin(args.expdir, 'model_epoch%d.h5' % epoch))

        # run on validation
        valid_costs = []
        valid_lengths = []

        for ss, sm, ts, tm in valid_stream.get_epoch_iterator():
            # just clip to avoid running out of memory (when using attention)
            ss, sm = pyrpad(ss, sm, args.rlayers, args.pyramid)
            ss, ts = ss.astype(np.int32), ts.astype(np.int32)
            sm, tm = sm.astype(np.bool), tm.astype(np.bool)

            # TODO make sure validation clipping doesn't hurting performance
            ss, sm = clip_lengths(ss, sm, CHAR_MAX_SEQ_LEN*2)
            rss = reverse_sent(ss, sm)
            # NOTE batches sometimes different sizes
            cost = encdec.test(ss, sm, rss, ts, tm, None)
            valid_costs.append(cost * tm.shape[0])
            total_length = np.sum(tm[:, 1:])
            valid_lengths.append(total_length)

            # sample / generate / save data for visualization

            if len(valid_costs) == 1:  # NOTE only generate from first batch
                samples = list()
                alignments = list()
                sources = list()
                for k in xrange(ss.shape[0]):  # over batch
                    ssk, smk, tsk, tmk = ss[k:k+1], sm[k:k+1], ts[k:k+1], tm[k:k+1]
                    rssk = reverse_sent(ssk, smk)
                    sslen = np.sum(smk)
                    if args.attention:
                        tmp = encdec.encode(ssk, rssk, smk, None)
                        _, hs = tmp[:-1], tmp[-1]
                        sample, alignment = sample_output(encdec, bdict, fdict, hs=hs)
                        samples.append(sample)
                        alignments.append(alignment)
                        sources.append(ssk)
                    else:
                        # FIXME this path is broken
                        h_t = encdec.encode(ssk, rssk, smk, None)
                        sample = sample_output(encdec, h_t, bdict, fdict)
                    logger.info(seq_to_str(ssk[0, 0:sslen].tolist(), bdict) + ' | ' + sample)
                with open(pjoin(args.expdir, 'alignments.pk'), 'wb') as fout:
                    pickle.dump({'sources': sources,
                                 'samples': samples,
                                 'alignments': alignments,
                                 'bdict': bdict}, fout)

        logger.info('validation cost: %f' %\
                (sum(valid_costs) / float(sum(valid_lengths))))
